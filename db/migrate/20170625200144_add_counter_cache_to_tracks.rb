class AddCounterCacheToTracks < ActiveRecord::Migration[5.1]
  def change
    add_column :tracks, :favourites_count, :integer, default: 0
  end
end
