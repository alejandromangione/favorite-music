class CreateTracks < ActiveRecord::Migration[5.1]
  def change
    create_table :tracks do |t|
      t.string :name
      t.belongs_to :style, index: true
      t.belongs_to :artist, index: true

      t.timestamps
    end
  end
end
