# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

u1 = User.create(first_name: 'John', last_name: 'Smith', email: 'user@email.com', password: 'password', birthday: Date.today)
u2 = User.create(first_name: 'Peter', last_name: 'Wall', email: 'admin@email.com', password: 'password', birthday: Date.today, role: "admin")

a = Artist.create(name: 'Queen')
s = Style.create(name: 'Classic Rock')
t = Track.create(name: 'We Will Rock You', style: s, artist: a)
# Favourite.create(track: t, user: u1)

a = Artist.create(name: 'Pink Floyd')
t = Track.create(name: 'Another Brick in the Wal', style: s, artist: a)
# Favourite.create(track: t, user: u1)

t = Track.create(name: 'Time', style: s, artist: a)
# Favourite.create(track: t, user: u1)

t = Track.create(name: 'Money', style: s, artist: a)
# Favourite.create(track: t, user: u1)

t = Track.create(name: 'Us and Them', style: s, artist: a)