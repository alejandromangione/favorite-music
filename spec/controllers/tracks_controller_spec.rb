require "rails_helper"

RSpec.describe TracksController, type: :controller do
  describe "GET #index" do
    it "responds successfully with an HTTP 200 status code" do

      sign_in User.create(first_name: 'Peter', last_name: 'Wall', email: 'admin@email.com', password: 'password', birthday: Date.today, role: "admin")

      get :index
      expect(response).to be_success
      expect(response).to have_http_status(200)
    end

  end
end