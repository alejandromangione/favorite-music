require 'rails_helper'

RSpec.feature "need login home page", type: :feature do
  scenario 'when user try go home' do
    visit root_path
    expect(page).to have_content 'Log in'
  end
end
