require 'rails_helper'

RSpec.feature "login home page", type: :feature do

  before :each do
    User.create(first_name: 'Peter', last_name: 'Wall', email: 'admin@email.com', password: 'password', birthday: Date.today, role: "admin")
  end

  scenario 'when user try go home' do
    visit '/users/sign_in'
    fill_in 'Email', with: 'admin@email.com'
    fill_in 'Password', with: 'password'
    click_button 'Log in'
    expect(page).to have_content 'Favourite Music'
  end
end
