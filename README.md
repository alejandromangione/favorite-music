# Favourite Music Project

- Ruby version 2.3.4

- Rails version 5.1.1

## Prerequisites

### Services

- Elasticsearch

[Install Elasticsearch](https://www.elastic.co/guide/en/elasticsearch/reference/current/setup.html). For Homebrew, use:


```
brew install elasticsearch
```


## Setup

- Clone this repository

```
git clone git@bitbucket.org:alejandromangione/favorite-music.git
```

- Go into the repository

```
cd favorite-music
```

- Install the required Gems

```
bundle install
```

- Start Elasticsearch serivice, for Homebrew, use:
```
brew services start elasticsearch
```

- Update database configurations in database.yml


- Database creation, initialization and sample content

```
rails db:drop db:create db:migrate db:seed
```

- Run the App

```
rails server
```

Open a browser window and navigate to http://localhost:3000/



### Credentials to login

#### Admin:

```
Email: admin@email.com
Password: password
```

#### Customer:

```
Email: user@email.com
Password: password
```