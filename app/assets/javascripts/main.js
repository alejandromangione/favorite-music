document.addEventListener("turbolinks:load", function() {

  $(".aside-menu-collapse").sideNav();

  $('.datepicker').pickadate({
    selectYears: 100,
    selectMonths: true,
    max: new Date()
  });

  $('select').material_select();

  if( $('#my-favourites-card').length ) {
    favourites.updateCard();
  }

  Materialize.updateTextFields();

});

