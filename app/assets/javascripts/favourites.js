var favourites = function(){

   var config = {
     favouritesUrl: '/home/favourites'
   };

   function disableBtn(track_id) {
     document.querySelector("[data-track='" + track_id +"'] [type='submit']").disabled = true 
   };
   
   function updateCard(){
      cleanCard();

      $.get(config.favouritesUrl, function(data) { 
        $.each( data, function( key, value ) {
          $('#my-favourites-card .collection').append('<li class="collection-item">' +  value + '</li>')
        });
      })
   };

   function cleanCard() {
      $('#my-favourites-card .collection li').remove();
   };

   return {
     updateCard:updateCard,
     disableBtn:disableBtn
   }
}();