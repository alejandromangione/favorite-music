class FavouritesController < ApplicationController

  def create

    @favourite = Favourite.find_or_create_by(track: Track.find(track_params), user: current_user)

    respond_to do |format|
      format.js
    end

  end

  private

    def track_params
      params.permit(:track_id).require(:track_id)
    end

end

