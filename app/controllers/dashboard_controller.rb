class DashboardController < ApplicationController
  before_action :check_permission

  def index
    @artists = Artist.all
    @styles = Style.all
    @tracks = Track.all
  end
end
