class HomeController < ApplicationController
  def index
    @tracks = Track.search(search_params) if params['search'].present?
  end

  def user_tracks_favourites
    tracks_favourites = current_user.tracks_favourites
    render :json => tracks_favourites.to_json
  end

  private 
    def search_params
      params.permit(:search).require(:search)
    end

end
