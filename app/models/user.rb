class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  enum role: { customer: 0, admin: 1 }

  has_many :favourites

  def tracks_favourites
    self.favourites.includes(:track).collect { |f| "#{f.track.artist.name} -  #{f.track.name}" }
  end

  def admin?
    self.role == "admin"
  end

end
