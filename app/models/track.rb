class Track < ApplicationRecord
  searchkick

  belongs_to :artist
  belongs_to :style

  has_many :favourites

  def is_favourite_by_user?(user)
    self.favourites.any? { |f| f.user == user}
  end

  def search_data
    {
      name: name,
      style: style.name,
      artist: artist.name
    }
  end

end
