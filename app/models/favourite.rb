class Favourite < ApplicationRecord
  belongs_to :track, counter_cache: true
  belongs_to :user

  before_save :update_counters

  private 
    def update_counters
      self.track.style.increment_favourite
      self.track.artist.increment_favourite
    end

end
