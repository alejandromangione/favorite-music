module Favouritable
  extend ActiveSupport::Concern

  def increment_favourite
    update_column(:favourites_count, favourites_count + 1)
  end

end