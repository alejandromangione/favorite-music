class Style < ApplicationRecord
  include Favouritable

  has_many :tracks
  has_many :favourites, through: :tracks

end
