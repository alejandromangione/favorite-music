Rails.application.routes.draw do

  get 'dashboard/index'

  resources :tracks
  resources :styles
  resources :artists
  resources :favourites, only: [:create]

  devise_for :users

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: "home#index"

  get 'home/index'
  get 'home/favourites', to: 'home#user_tracks_favourites'

end
